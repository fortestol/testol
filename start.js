var express = require('express');
	app = express();
	http = require('http');
	login = require('./index');
	//getUnit = require('./getUnit');

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(app.router);
app.use(express.static(__dirname+'/public'));
app.use(express.errorHandler());

app.get('/', login);


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});